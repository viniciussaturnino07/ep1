# EP1 - OO 2019.2 (UnB - Gama)

## Nome: Vinicius de Sousa Saturnino
## Matricula: 18/0132245

Turmas Carla
Data de entrega: 01/10/2019

## Bibliotecas necessárias

- iostream
- fstream
- string
- stdbool.h
- vector

Esse projeto foi desenvolvido na linguagem C++ e possui a seguinte estrutura de interface no terminal:

- **Interface inicial:** Possui 3 opções além da opção de sair sendo elas: (1)Modo Venda, (2)Modo Estoque e (3)Modo Recomendação. De acordo com a escolha do usuário é aberto um novo menu, referente a cada um dos modos.

## Modo Venda
Antes de iniciar o Menu do Modo Venda, é feita uma verificação se o cliente já é ou não cadastrado. Caso seja cadastrado, inicia-se o Modo Venda, caso contrário, inicia-se o cadastro do cliente que será adicionado a um arquivo socio.txt.
**Menu de Vendas:** Nesse menu temos as opções de adicionar produto ao carrinho, remover o produto do carrinho, mostrar o carrinho, mostrar o estoque de produtos disponiveis, finalizar a compra e voltar ao menu anterior.

## Modo Estoque
**Menu de Estoque:** Nesse menu temos as opções de adicionar produto ao estoque (criando um arquivo estoque.txt), Mostrar o estoque, adicionar uma nova categoria (criando um arquivo categoria.txt), mostrar as categorias, atualizar a quantidade de produtos no estoque (reposição de mercadoria) e voltar ao menu anterior.

## Modo Recomendação
Nesse modo é recomendado os produtos para o cliente de acordo com as suas compras anteriores.

