#ifndef CLIENTE_HPP
#define CLIENTE_HPP
#include <iostream>
#include <string>
using namespace std;

  class Cliente{
    private:
      //Atributos
      string cpf;
    public:
      //Construtores e Destrutor
      Cliente();
      Cliente(string cpf);
      ~Cliente();

      //Metodos
      void set_cpf(string cpf);
      string get_cpf();
};

#endif
