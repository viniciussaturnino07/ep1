#ifndef CARRINHO_HPP
#define CARRINHO_HPP
#include <iostream>
#include "produto.hpp"
#include "estoque.hpp"
#include <vector>
#include <stdbool.h>
using namespace std;

class Carrinho:public Estoque{
  private:
    //Atributos
    vector <Produto*> lista_de_produtos;
    int tamanhoCarrinho; //quantidade de produtos dentro do carrinho
    float preco_final;
  public:
    //Construtores e Destrutor
    Carrinho();
    Carrinho(int tamanhoCarrinho);
    ~Carrinho();

    //Metodos
    vector<Produto*> get_lista_de_produtos();
    void set_preco_final();
    float get_preco_final();
    void set_tamanhoCarrinho();
    int get_tamanhoCarrinho();
    void cadastraProduto();
    void removeProduto();
    bool efetuaPagamento();
};

#endif
