#ifndef ESTOQUE_HPP
#define ESTOQUE_HPP
#include <iostream>
#include <vector>
#include <string>
#include <stdbool.h>
#include "produto.hpp"

class Estoque{
  private:
    vector<string> categoria;
    vector<Produto*> estoqueProdutos;
    int tamanhoEstoque;
    int tamanhoCategoria;
  public:
    //Construtor e Destrutor
    Estoque();
    ~Estoque();

    //Metodos
    void set_estoque();
    void set_categoria();
    void get_categoria();
    void set_tamanhoCategoria();
    int get_tamanhoCategoria();
    void set_tamanhoEstoque();
    int get_tamanhoEstoque();
    void get_estoqueProdutos();
    Produto* get_produtoEstoque(string produto);

    vector <Produto*> returnEstoque();
    bool verificaEstoque();
    void arquivoEstoque();
    bool verificaCategoria();
    void carregaCategoria();
    void arquivoCategoria();
    void mudaQuantidade();
    void mudaQuantidade(string produto, int quantidade);
    virtual void cadastraProduto();
};

#endif
