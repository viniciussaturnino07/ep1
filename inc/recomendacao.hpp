#ifndef RECOMENDACAO_HPP
#define RECOMENDACAO_HPP
#include <iostream>
#include <vector>
#include "produto.hpp"
#include <stdbool.h>
#include <string>

class Recomendacao{
  public:
    //Atributos
    int tamanhoCategoria;
    int tamanhoLista;
    string produto;
    vector<Produto*> listaRecomendados;
    vector<string> categoria;
    vector <int> conta;
    vector <Produto*> estoque;
  public:
    //Construtores e Destrutor
    Recomendacao();
    Recomendacao(vector <Produto*> listaRecomendados);
    Recomendacao(string produto);
    ~Recomendacao();

    //Metodos
    void concatenar(string produto);
    void carrega_listaProdutos(vector <Produto*> listaRecomendados);
    void set_tamanhoCategoria();
    void arquivoRecomendacao();
    void carregaArquivo();
    bool verificaArquivo();
    void define_listaCategoria();
    void set_tamanhoLista();
    void recomendar();
    void ordena();
    void carregaEstoque(vector <Produto*> estoque);
};

#endif
