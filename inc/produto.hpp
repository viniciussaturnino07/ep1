#ifndef PRODUTO_HPP
#define PRODUTO_HPP
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
using namespace std;

  class Produto{
    private:
      //Atributos
      int quantidade, tamanhoCategoria;
      string produto;
      float preco;
      vector <string> vectorCategoria;
    public:
      //Construtores e Destrutor
      Produto();
      Produto(string produto, vector <string> vectorCategoria, float preco, int quantidade);
      ~Produto();

      //Metodos
      void set_quantidade(int quantidade);
      int get_quantidade();
      void set_produto(string produto);
      string get_produto();
      void set_categoria(vector <string> categoria);
      string get_categoria(int i);
      void set_preco(float preco);
      float get_preco();
      void set_tamanhoCategoria();
      int get_tamanhoCategoria();
      vector <string> get_vectorCategoria();
};

#endif
