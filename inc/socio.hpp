#ifndef SOCIO_HPP
#define SOCIO_HPP
#include <iostream>
#include <string>
#include <vector>
#include "cliente.hpp"
using namespace std;

class Socio:public Cliente{
  private:
    //Atributos
    string email;
    string nome;
    string telefone;
    string endereco;
    string sexo;
    int t_socio;
    vector <Socio*> socio;
  public:
    //Construtores e Destrutor
    Socio();
    Socio(string nome, string endereco, string email, string cpf, string telefone, string sexo);
    ~Socio();

    //Metodos
    void set_email(string email);
    string get_email();
    void set_nome(string nome);
    string get_nome();
    void set_telefone(string telefone);
    string get_telefone();
    void set_endereco(string endereco);
    string get_endereco();
    void set_sexo(string sexo);
    string get_sexo();
    void adicionaSocio();
    void set_tamanho_socio();
    void set_socio();
    void arquivoSocio();
    bool verificaCPF();
    bool verificaSocio();
};

#endif
