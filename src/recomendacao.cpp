#include "recomendacao.hpp"
#include "produto.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <stdbool.h>

Recomendacao::Recomendacao(){
  carregaArquivo();
  set_tamanhoLista();
  set_tamanhoCategoria();
}

Recomendacao::Recomendacao(vector <Produto*> listaRecomendados){
  carregaArquivo();
  carrega_listaProdutos(listaRecomendados);
  set_tamanhoLista();
  set_tamanhoCategoria();
}

Recomendacao::Recomendacao(string produto){
  concatenar(produto);
  carregaArquivo();
  set_tamanhoLista();
  set_tamanhoCategoria();
}

Recomendacao::~Recomendacao(){}

void Recomendacao::set_tamanhoCategoria(){
    this->tamanhoCategoria = int(this->categoria.size());
}

void Recomendacao::concatenar(string cpf){
    string txt, final = ".txt";
    txt = cpf+final;
    this->produto = txt;
}

void Recomendacao::carrega_listaProdutos(vector <Produto*> listaRecomendados){
    this->listaRecomendados = listaRecomendados;
}

void Recomendacao::set_tamanhoLista(){
    this->tamanhoLista=int(this->listaRecomendados.size());
}

void Recomendacao::carregaEstoque(vector <Produto*> estoque){
    this->estoque = estoque;
}

//carrega txt estoque
void Recomendacao::carregaArquivo(){
    if(verificaArquivo()==true){
        string categorias,tamanho,quantidade;
        ifstream arquivo;
        int tam,qtd;
        arquivo.open("arquivos/"+this->produto);
        getline(arquivo,tamanho);
        tam = atoi(tamanho.c_str());
        for(int i=0; i<tam; i++){
            vector <string> categoria;
            getline(arquivo,categorias);
            getline(arquivo,quantidade);
            qtd = atoi(quantidade.c_str());
            this->categoria.push_back(categorias);
            this->conta.push_back(qtd);
        }
        arquivo.close();
    }
    else if(verificaArquivo() == false){}
}

void Recomendacao::define_listaCategoria(){
    for(int i=0; i<this->tamanhoLista; i++){
        for(int j=0; j<this->listaRecomendados[i]->get_tamanhoCategoria(); j++){
            set_tamanhoCategoria();
            if(this->tamanhoCategoria==0){
                this->categoria.push_back(this->listaRecomendados[i]->get_categoria(j));
                this->conta.push_back(1);
                this->tamanhoCategoria++;
            }
            else{
                int index=0;
                for(int k=0; k<this->tamanhoCategoria; k++){
                    if(this->categoria[k] == this->listaRecomendados[k]->get_categoria(j)){
                        this->conta[k]++;
                        index++;
                    }
                }
                if(index==0){
                    this->categoria.push_back(this->listaRecomendados[i]->get_categoria(j));
                    this->conta.push_back(1);
                    this->tamanhoCategoria++;
                }
            }
        }
    }
}


//Ordenação das categorias
void Recomendacao::ordena(){
    if(this->tamanhoCategoria==0)
        cout << "Não é possível fazer recomendações" << endl;
    else{
        for(int i=0; i<this->tamanhoCategoria; i++)
            for(int j=0; j<this->tamanhoCategoria; j++)
                if(this->conta[i]<this->conta[j]){
                    int index = this->conta[j];
                    this->conta[j] = this->conta[i];
                    this->conta[i] = index;
                    string categoria = this->categoria[j];
                    this->categoria[j] = this->categoria[i];
                    this->categoria[i] = categoria;
                }
              }
            }

void Recomendacao::arquivoRecomendacao(){
    ofstream arquivo;
    arquivo.open("arquivos/"+this->produto);
    arquivo << this->tamanhoCategoria << endl;
    for(int i=0; i<this->tamanhoCategoria; i++){
        arquivo << categoria[i] << endl;
        arquivo << conta[i] << endl;
    }
    arquivo.close();
}

//verifica se existe txt
bool Recomendacao::verificaArquivo(){
    ifstream arquivo;
    arquivo.open("arquivos/"+this->produto);
    if(arquivo.is_open()){
        arquivo.close();
        return true;
    }
    else{
        return false;
    }
}

void Recomendacao::recomendar(){
    ordena();
    int cont=0;
    cout << "Produtos recomendados: " << endl;
    for(int i=this->tamanhoCategoria-1; i>=0; i--){
        cont++;
        vector <string> recomendado;
        for(int j=0; j<int(this->estoque.size()); j++){
            for(int k=0; k<int(this->estoque[j]->get_tamanhoCategoria()); k++){
                if(this->categoria[i]==this->estoque[j]->get_categoria(k)){
                    recomendado.push_back(this->estoque[j]->get_produto());
                }
            }
        }
        if(cont==1){
            if(int(recomendado.size()<5)){
                for(int w=0;w<int(recomendado.size());w++){
                    int tam = int(recomendado.size());
                    int randomizado = rand()%tam;
                    std::cout << recomendado[randomizado] << endl;
                    recomendado.erase(recomendado.begin()+randomizado);
                }
            }
            else{
                for(int w=0; w<5; w++){
                    int tam = int(recomendado.size());
                    int randomizado = rand()%tam;
                    cout << recomendado[randomizado] << endl;
                    recomendado.erase(recomendado.begin()+randomizado);
                }
            }
        }
        if(cont==2){
            if(int(recomendado.size()<3)){
                for(int w=0; w<int(recomendado.size()); w++){
                    int tam = int(recomendado.size());
                    int randomizado=rand()%tam;
                    std::cout << recomendado[randomizado] << endl;
                    recomendado.erase(recomendado.begin()+randomizado);
                }
            }
            else{
                for(int w=0; w<3; w++){
                    int tam=int(recomendado.size());
                    int randomizado=rand()%tam;
                    std::cout << recomendado[randomizado] << endl;
                    recomendado.erase(recomendado.begin()+randomizado);
                }
            }
        }
        if(cont==3){
            if(int(recomendado.size()<2)){
                for(int w=0; w<int(recomendado.size()); w++){
                    int tam=int(recomendado.size());
                    int randomizado=rand()%tam;
                    std::cout << recomendado[randomizado] << endl;
                    recomendado.erase(recomendado.begin()+randomizado);
                }
            }
            else{
                for(int w=0; w<2; w++){
                    int tam = int(recomendado.size());
                    int randomizado = rand()%tam;
                    std::cout << recomendado[randomizado] << endl;
                    recomendado.erase(recomendado.begin()+randomizado);
                }
            }
        }
    }
}
