#include "socio.hpp"
#include "cliente.hpp"
#include <fstream>
#include <iostream>
#include <string>
#include <stdbool.h>

Socio::Socio(){
  set_nome("");
  set_endereco("");
  set_email("");
  set_cpf("");
  set_telefone("");
  set_sexo("");
  set_tamanho_socio();
  set_socio();
}//

Socio::Socio(string nome, string endereco, string email, string cpf, string telefone, string sexo){
  set_nome(nome);
  set_endereco(endereco);
  set_email(email);
  set_cpf(cpf);
  set_telefone(telefone);
  set_sexo(sexo);
}

Socio::~Socio(){}

//get e set do email
void Socio::set_email(string email){
  this->email = email;
}
string Socio::get_email(){
  return email;
}

//get e set do telefone
void Socio::set_telefone(string telefone){
  this->telefone = telefone;
}
string Socio::get_telefone(){
  return telefone;
}

//get e set do endereco
void Socio::set_endereco(string endereco){
  this->endereco = endereco;
}
string Socio::get_endereco(){
  return endereco;
}

//get e set do cpf
void Socio::set_nome(string nome){
  this->nome = nome;
}
string Socio::get_nome(){
  return nome;
}

//get e set do sexo
void Socio::set_sexo(string sexo){
  this->sexo = sexo;
}
string Socio::get_sexo(){
  return sexo;
}

//Função que recebe o numero de socios
void Socio::set_tamanho_socio(){
  ifstream socio;
  string t_socio;
  socio.open("arquivo/socio.txt");
  if(socio.is_open()){
    getline(socio,t_socio);
    this->t_socio=atoi(t_socio.c_str());
    socio.close();
  }
  else{
    this->t_socio = 0;
  }
}

bool Socio::verificaCPF(){
  string cpf;
  cout << "Digite seu cpf: " << endl;
  getline(cin,cpf);
  for(int i=0;i<this->t_socio;i++){
    if(cpf==this->socio[i]->get_cpf()){
      cout<<"Socio já existente"<<endl;
      set_cpf(cpf);
      cout << "Aperte enter para continuar..." << endl;
      return true;
    }
  }
  cout << "Socio não existente" << endl;
  cout << "Aperte enter para continuar" << endl;
  return false;
}

//Função que verifica se o txt existe
bool Socio::verificaSocio(){
  ifstream socio;
  socio.open("arquivo/socio.txt");
  if(socio.is_open()){
    socio.close();
    return true;
  }
  else{
    return false;
  }
}

//Função que adiciona os elementos do arquivo txt em um vector
void Socio::set_socio(){
  if(verificaSocio()==true){
    string lixo;
    string nome, endereco, email, cpf, telefone, sexo;
    ifstream socio;
    socio.open("arquivo/socio.txt");
    getline(socio, lixo);
    Socio* lista_de_socios;
    for(int i=0; i<this->t_socio; i++){
      getline(socio,nome);
      getline(socio,endereco);
      getline(socio,email);
      getline(socio,cpf);
      getline(socio,telefone);
      getline(socio,sexo);

      lista_de_socios = new Socio(nome,endereco,email,cpf,telefone,sexo);
      this->socio.push_back(lista_de_socios);
    }
    socio.close();
  }
  else if(verificaSocio()==false){
    std::cout << "Sem socios" << endl;
  }
}

//Função que cadastra cliente como sócio
void Socio::adicionaSocio(){
  string nome, endereco, email, cpf, telefone, sexo;
  /*O dado usado para validar se o cliente é sócio ou não
  é o CPF, pois podem existir clientes com mesmo Nome,
  CPF é um dado único de cada pessoa.*/
  std::cout << "Digite o seu cpf: " << endl;
  std::cin >> cpf;
  while(cpf.size()!=11){
    std::cin >> cpf;
  }
  for(int i=0; i<this->t_socio; i++){
    if(cpf==this->socio[i]->get_cpf()){
      std::cout << "Cliente já cadastrado" << endl;
      return;
    }
  }
  std::cout << "** Cliente não encontrado **" << endl;
  std::cout << "Iniciando cadastro . . ." << endl;
  std::cout << "Digite seu nome: " << endl;
  cin.ignore();
  getline(cin, nome);
  std::cout << "Digite seu endereço: " << endl;
  getline(cin, endereco);
  std::cout << "Digite seu email: " << endl;
  std::cin >> email;
  std::cout << "Digite seu telefone: " << endl;
  std::cin >> telefone;
  std::cout << "Sexo ['M'/'F']: " << endl;
  std::cin >> sexo;

  Socio *lista_de_socios = new Socio(nome,endereco,email,cpf,telefone,sexo);
  this->socio.push_back(lista_de_socios);
  cout << "Cliente cadastrado com sucesso" << endl;
  this->t_socio++;
  cin.ignore();
  getchar();
}

//Função que adiciona os Clientes a um arquivo .txt
void Socio::arquivoSocio(){
    if(verificaSocio()==true||this->socio.size()!=0){
        ofstream socio;
        socio.open("arquivo/socio.txt");
        socio << this->t_socio << endl;
        for(int i=0; i<this->t_socio; i++){
            socio << this->socio[i]->get_nome() << endl;
            socio << this->socio[i]->get_endereco() << endl;
            socio << this->socio[i]->get_email() << endl;
            socio << this->socio[i]->get_cpf() << endl;
            socio << this->socio[i]->get_telefone() << endl;
            socio << this->socio[i]->get_sexo() << endl;
        }
        socio.close();
    }
    else{}
}
