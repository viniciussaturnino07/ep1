#include "estoque.hpp"
#include "produto.hpp"
#include <iostream>
#include <stdbool.h>
#include <vector>
#include <fstream>
#include <string>

Estoque::Estoque(){
    set_tamanhoEstoque();
    set_estoque();
    set_tamanhoCategoria();
    carregaCategoria();
}

Estoque::~Estoque(){}

//Funções para o tamanho do estoque
void Estoque::set_tamanhoEstoque(){
    ifstream estoque;
    string tamanhoEstoque;
    estoque.open("arquivo/estoque.txt");
    if(estoque.is_open()){
        getline(estoque,tamanhoEstoque);
        this->tamanhoEstoque=atoi(tamanhoEstoque.c_str());
        estoque.close();
    }
    else{
        this->tamanhoEstoque=0;
    }
}
int Estoque::get_tamanhoEstoque(){
    return this->tamanhoEstoque;
}

//retorna produtos e seus atributos
void Estoque::get_estoqueProdutos(){
    for(int i=0; i<this->tamanhoEstoque; i++){
        std::cout << "Nome: " << this->estoqueProdutos[i]->get_produto()<<endl;
        std::cout << "Categoria: ";
        for(int j=0;j<this->estoqueProdutos[i]->get_tamanhoCategoria();j++){
          std::cout << this->estoqueProdutos[i]->get_categoria(j) << "|";
        }
        std::cout << endl << "Valor: R$" << this->estoqueProdutos[i]->get_preco() << endl;
        std::cout << "Quantidade: "<<this->estoqueProdutos[i]->get_quantidade() << endl;
        std::cout << "----------------------------------------------------" << endl;
    }
}

//Função de busca de produto no estoque
Produto* Estoque::get_produtoEstoque(string produto){
    for(int i=0; i<this->tamanhoEstoque; i++){
        if(produto==estoqueProdutos[i]->get_produto()){
            Produto* meuProduto = new Produto(estoqueProdutos[i]->get_produto(),estoqueProdutos[i]->get_vectorCategoria(),estoqueProdutos[i]->get_preco(),estoqueProdutos[i]->get_quantidade());
            return meuProduto;
        }
    }
    std::cout << "****** Produto inexistente no estoque ******" << endl;
    return NULL;
}

//carrega txt
void Estoque::set_estoque(){
    if(verificaEstoque()==true){
        string nome, categoria, preco, quantidade, lixo, tCategoria;
        ifstream estoque;
        int qtd,tamanhoCategoria;
        float valor;
        estoque.open("arquivo/estoque.txt");
        getline(estoque,lixo);
        Produto* meuProduto;
        for(int i=0; i<this->tamanhoEstoque; i++){
            vector <string> categorias;
            getline(estoque,nome);
            getline(estoque,tCategoria);
            tamanhoCategoria=atoi(tCategoria.c_str());
            for(int j=0; j<tamanhoCategoria; j++){
                getline(estoque,categoria);
                categorias.push_back(categoria);
            }
            getline(estoque,preco);
            getline(estoque,quantidade);
            qtd=atoi(quantidade.c_str());
            valor=atof(preco.c_str());
            meuProduto=new Produto(nome,categorias,valor,qtd);
            meuProduto->set_tamanhoCategoria();
            this->estoqueProdutos.push_back(meuProduto);
        }
        estoque.close();
    }
    if(verificaEstoque()==false){
    }
}

//verifica se existe txt
bool Estoque::verificaEstoque(){
    ifstream estoque;
    estoque.open("arquivo/estoque.txt");
    if(estoque.is_open()){
        estoque.close();
        return true;
    }
    else{
        return false;
    }
}

//Função que cadastra produto
void Estoque::cadastraProduto(){
    vector <string> categorias;
    string nome,categoria;
    int quantidade, tamanhoCategoria , cont=0;
    float valor;

    std::cout << "Digite o nome do produto: " << endl;
    cin.ignore();
    getline(cin,nome);
    for(int i=0; i<this->tamanhoEstoque; i++){
        if(nome==this->estoqueProdutos[i]->get_produto()){
            std::cout << "Produto já cadastrado" << endl;
            return;
        }
    }
    std::cout << endl << "Produto não cadastado" << endl;
    std::cout << "Iniciando cadastro. . ." << endl;
    std::cout << endl << "Qual a quantidade de categorias desse produto?" << endl;
    std::cin>>tamanhoCategoria;
    cin.ignore();
    for(int i=0; i<tamanhoCategoria; i++){
        std::cout << endl << "Categoria "<< i+1 <<": " << endl;
        getline(cin,categoria);
        categorias.push_back(categoria);
    }
    std::cout << endl << "Valor do Produto: " << endl;
    std::cin >> valor;
    std::cout << endl << "Quantidade no estoque: " << endl;
    cin.ignore();
    std::cin >> quantidade;
    for(int i=0; i<this->tamanhoCategoria; i++){
        for(int j=0; j<tamanhoCategoria; j++){
            if(categorias[j]==this->categoria[i]){
                cont++;
            }
        }
    }
    if(cont==tamanhoCategoria){
        Produto* meuProduto = new Produto(nome,categorias,valor,quantidade);
        meuProduto->set_tamanhoCategoria();
        this->estoqueProdutos.push_back(meuProduto);
        this->tamanhoEstoque++;
        std::cout << "Produto cadastrado com sucesso!" << endl;
        return;
    }
    std::cout << "****** Categoria não existente ******" << endl;
}

//Função que cria arquivo txt
void Estoque::arquivoEstoque(){
    if(verificaEstoque()==true||this->estoqueProdutos.size()!=0){
        ofstream estoque;
        estoque.open("arquivo/estoque.txt");
        estoque << this->tamanhoEstoque << endl;
        for(int i=0; i<this->tamanhoEstoque; i++){
            estoque << this->estoqueProdutos[i]->get_produto() << endl;
            estoque << this->estoqueProdutos[i]->get_tamanhoCategoria() << endl;
            for(int j=0; j<this->estoqueProdutos[i]->get_tamanhoCategoria(); j++){
              estoque << this->estoqueProdutos[i]->get_categoria(j) << endl;
            }
            estoque << this->estoqueProdutos[i]->get_preco()<<endl;
            estoque << this->estoqueProdutos[i]->get_quantidade()<<endl;
        }
        estoque.close();
    }else{
    }
}

//salva arquivo txt das categorias.
void Estoque::arquivoCategoria(){
    if(verificaCategoria()==true||categoria.size()!=0){
        ofstream categorias;
        categorias.open("arquivo/categorias.txt");
        categorias << this->tamanhoCategoria << endl;
        for(int i=0; i<this->tamanhoCategoria; i++){
            categorias << this->categoria[i] << endl;
        }
        categorias.close();
    }
    else{}
}

//verifica existencia do txt
bool Estoque::verificaCategoria(){
    ifstream categorias;
    categorias.open("arquivo/categorias.txt",ios::in);
    if(categorias.is_open()){
        categorias.close();
        return true;
    }
    else{
        return false;
    }
}

//Função que carrega o arquivo txt
void Estoque::carregaCategoria(){
    if(verificaCategoria()==true){
        string categoria,lixo;
        ifstream categorias;
        categorias.open("arquivo/categorias.txt",ios::in);
        getline(categorias,lixo);
        for(int i=0; i<this->tamanhoCategoria; i++){
            getline(categorias,categoria);
            this->categoria.push_back(categoria);
        }
        categorias.close();
    }
    if(verificaCategoria()==false){
    }
}


//get e set da categoria.
void Estoque::set_categoria(){
    string categoria;
    std::cout << "Digite o nome da categoria a ser adicionada" << endl;
    cin.ignore();
    getline(cin,categoria);
    this->categoria.push_back(categoria);
    std::cout << "Categoria adicionada com sucesso!" << endl;
    this->tamanhoCategoria++;
}
void Estoque::get_categoria(){
    for(int i=0; i<this->tamanhoCategoria; i++){
        std::cout << this->categoria[i] << endl;
    }
}

//Função que calcula o número de categorias
int Estoque::get_tamanhoCategoria(){
    return this->tamanhoCategoria;
}

//Função que define quantidade de categorias.
void Estoque::set_tamanhoCategoria(){
    ifstream categorias;
    string t_categorias;
    categorias.open("arquivo/categorias.txt",ios::in);
    if(categorias.is_open()){
        getline(categorias,t_categorias);
        this->tamanhoCategoria = atoi(t_categorias.c_str());
        categorias.close();
    }
    else{
        this->tamanhoCategoria = 0;
    }
}

//Função para alterar a quantidade de um determinado produto.
void Estoque::mudaQuantidade(){
    string nome;
    int quantidade;
    std::cout<<"Digite o produto: " << endl;
    cin.ignore();
    getline(cin,nome);
    for(int i=0; i<this->tamanhoEstoque; i++){
        if(nome==this->estoqueProdutos[i]->get_produto()){
            std::cout << "Digite a quantidade: ";
            std::cin >> quantidade;
            estoqueProdutos[i]->set_quantidade(quantidade);
            cout << "Alteração realizada com sucesso" << endl;
            return;
        }
    }
    std::cout << "Produto não encontrado" << endl;
}


//Função que diminui quantidade após compra
void Estoque::mudaQuantidade(string produto,int quantidade){
    for(int i=0; i<this->tamanhoEstoque; i++){
        if(produto==this->estoqueProdutos[i]->get_produto()){
            estoqueProdutos[i]->set_quantidade(estoqueProdutos[i]->get_quantidade()-quantidade);
            return;
        }
    }
}

vector <Produto*> Estoque::returnEstoque(){
    return this->estoqueProdutos;
}
