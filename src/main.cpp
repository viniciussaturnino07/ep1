#include "cliente.hpp"
#include "socio.hpp"
#include "produto.hpp"
#include "carrinho.hpp"
#include "estoque.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include "recomendacao.hpp"

int main(){
  int operacao,operacao1,aux;
  string nome,endereco,email,cpf,telefone,sexo;
  Socio* lista_de_socios=new Socio();
  Produto prod;
  string produto;
  bool end=true;
  do{

    system("clear");

    std::cout << "------ BEM VINDO A NOSSA LOJA ------" << endl;
    std::cout << "------------------------------------" << endl;
    std::cout << "O que deseja fazer?" << endl << endl;
    std::cout << "(1) Modo Venda" << endl;
    std::cout << "(2) Modo Estoque" << endl;
    std::cout << "(3) Modo Recomendação" << endl;
    std::cout << "(0) Sair" << endl;
    std::cin >> operacao;
    while(operacao!=0&&operacao!=1&&operacao!=2&&operacao!=3){
      std::cin >> operacao;
    }
    cin.ignore();
    system("clear");
    switch(operacao){
    case 1:{
      lista_de_socios->adicionaSocio();
      lista_de_socios->arquivoSocio();
      getchar();
      Carrinho *meuCarrinho = new Carrinho();
        std::cout << "Aperte enter para continuar" << endl;
        getchar();
        do {
          aux=0;
          std::cout << "------ MODO VENDA ------" << endl << endl;
          std::cout << "(1) Adicionar produto ao carrinho" << endl;
          std::cout << "(2) Remover produto do carrinho" << endl;
          std::cout << "(3) Mostrar carrinho" << endl;
          std::cout << "(4) Mostrar estoque de produtos" << endl;
          std::cout << "(5) Finalizar Compra" << endl;
          std::cout << "(0) Voltar" << endl;
          std::cin >> operacao1;
          while(operacao1!=0&&operacao1!=1&&operacao1!=2&&operacao1!=3&&operacao1!=4&&operacao1!=5){
            std::cin >> operacao1;
          }
          switch(operacao1){
            case 1:{
              system("clear");
              meuCarrinho->cadastraProduto();
              std::cout << "Aperte enter para continuar" << endl;
              getchar();
              break;
            }
            case 2:{
              system("clear");
              meuCarrinho->removeProduto();
              std::cout << "Aperte enter para continuar" << endl;
              getchar();
              break;
            }
            case 3:{
              system("clear");
              meuCarrinho->get_lista_de_produtos();
              cin.ignore();
              std::cout << "Aperte enter para continuar" << endl;
              getchar();
              break;
            }
            case 4:{
              system("clear");
              meuCarrinho->get_estoqueProdutos();
              cin.ignore();
              std::cout << "Aperte enter para continuar" << endl;
              getchar();
              break;
            }
            case 5:{
              system("clear");
              end=meuCarrinho->efetuaPagamento();
              if(end==false){
                aux=0;
              }
              else{
                aux=1;
                Recomendacao *recomendacao=new Recomendacao(meuCarrinho->get_lista_de_produtos());
                recomendacao->concatenar(lista_de_socios->get_cpf());
                recomendacao->define_listaCategoria();
                recomendacao->arquivoRecomendacao();
                getchar();
              }
              break;
            }
          }
        } while(operacao1==1||operacao1==2||operacao1==3||operacao1==4||(operacao1!=0&&aux==0));
        meuCarrinho->arquivoEstoque();
        meuCarrinho->arquivoCategoria();
        break;
      }
    case 2:{
      Estoque *estoqueProdutos = new Estoque();
    do{
      system("clear");
      std::cout << "------ MODO ESTOQUE -------" << endl;
      std::cout << "(1) Adicionar produto ao estoque" << endl;
      std::cout << "(2) Mostrar produtos do estoque" << endl;
      std::cout << "(3) Adicionar categoria de produtos" << endl;
      std::cout << "(4) Mostrar categorias" << endl;
      std::cout << "(5) Atualizar quantidade no estoque" << endl;
      std::cout << "(0) Voltar" << endl;
      std::cin >> operacao1;
      while(operacao1!=0&&operacao1!=1&&operacao1!=2&&operacao1!=3&&operacao1!=4&&operacao1!=5){
        std::cin >> operacao1;
      }
      switch(operacao1){
        case 1:{
          system("clear");
          estoqueProdutos->cadastraProduto();
          estoqueProdutos->arquivoEstoque();
          std::cout << "Aperte enter para continuar" << endl;
          cin.ignore();
          getchar();
          break;
        }
        case 2:{
          system("clear");
          estoqueProdutos->get_estoqueProdutos();
          cin.ignore();
          std::cout << "Aperte enter para continuar" << endl;
          getchar();
          break;
        }
        case 3:{
          system("clear");
          estoqueProdutos->set_categoria();
          cin.ignore();
          std::cout << "Aperte enter para continuar" << endl;
          getchar();
          break;
        }
        case 4:{
          system("clear");
          estoqueProdutos->get_categoria();
          estoqueProdutos->arquivoCategoria();
          cin.ignore();
          std::cout << "Aperte enter para continuar" << endl;
          getchar();
          break;
        }
        case 5:{
          estoqueProdutos->mudaQuantidade();
          std::cout << "Aperte enter para continuar" << endl;
          getchar();
          break;
        }
      }
    } while(operacao1==1||operacao1==2||operacao1==3||operacao1==4||operacao1==5);
        estoqueProdutos->arquivoEstoque();
        estoqueProdutos->arquivoCategoria();
        break;
      }
    case 3:{
      aux = lista_de_socios->verificaCPF();
      getchar();
      if(aux==true){
        Estoque *meuEstoque = new Estoque();
        Recomendacao *recomendacao = new Recomendacao(lista_de_socios->get_cpf());
        recomendacao->carregaEstoque(meuEstoque->returnEstoque());
        recomendacao->recomendar();
        getchar();
      }
      else{}
      break;
      }
    }
  }while(operacao==1||operacao==2||operacao==3);

  return 0;
}
