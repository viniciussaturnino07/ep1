#include "produto.hpp"
#include <iostream>
#include <fstream>
#include <string>

Produto::Produto(){
  set_produto("");
  set_preco(0);
  set_quantidade(0);
}

Produto::Produto(string produto, vector<string> vectorCategoria, float preco, int quantidade){
  set_produto(produto);
  set_categoria(vectorCategoria);
  set_preco(preco);
  set_quantidade(quantidade);
  set_tamanhoCategoria();
}

Produto::~Produto(){}

//get e set do produto
void Produto::set_produto(string produto){
  this->produto = produto;
}
string Produto::get_produto(){
  return produto;
}

//get e set da quantidade
void Produto::set_quantidade(int quantidade){
  this->quantidade = quantidade;
}
int Produto::get_quantidade(){
  return quantidade;
}

//get e set do preco
void Produto::set_preco(float preco){
  this->preco = preco;
}
float Produto::get_preco(){
  return preco;
}

//get e set da categoria
void Produto::set_categoria(vector<string> vectorCategoria){
  this->vectorCategoria = vectorCategoria;
}
string Produto::get_categoria(int i){
  return vectorCategoria[i];
}

//get e set do tamanho da categoria
void Produto::set_tamanhoCategoria(){
  this->tamanhoCategoria = vectorCategoria.size();
}
int Produto::get_tamanhoCategoria(){
  return tamanhoCategoria;
}

vector<string> Produto::get_vectorCategoria(){
  return this->vectorCategoria;
}
