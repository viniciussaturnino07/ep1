#include "carrinho.hpp"
#include "estoque.hpp"
#include "cliente.hpp"
#include <iostream>
using namespace std;

Carrinho::Carrinho(){
  set_preco_final();
  set_tamanhoEstoque();
  set_estoque();
  set_tamanhoCarrinho();
}

Carrinho::~Carrinho(){}

//set e get do tamanho da lista de produtos
void Carrinho::set_tamanhoCarrinho(){
  this->tamanhoCarrinho = this->lista_de_produtos.size();
}
int Carrinho::get_tamanhoCarrinho(){
  return tamanhoCarrinho;
}

//Recebe o nome dos produtos existentes no carrinho
vector <Produto*> Carrinho::get_lista_de_produtos(){
    for(int i=0;i<this->tamanhoCarrinho;i++){
        std::cout << "Nome: "<< this->lista_de_produtos[i]->get_produto() << endl;
        std::cout << "Quantidade: "<< this->lista_de_produtos[i]->get_quantidade() << endl;
        for(int j=0; j<this->lista_de_produtos[i]->get_tamanhoCategoria(); j++){
            std::cout << "Categoria: " << this->lista_de_produtos[i]->get_categoria(j) << endl;
        }
        std::cout << "----------------------------------------------------------" << endl;
    }
    return lista_de_produtos;
}

//get e set das funções de soma dos produtos do carrinho
void Carrinho::set_preco_final(){
    if(this->tamanhoCarrinho!=0){
      this->preco_final=0;
      for(int i=0; i<this->tamanhoCarrinho; i++){
        this->preco_final+=this->lista_de_produtos[i]->get_preco()*this->lista_de_produtos[i]->get_quantidade();
      }
    }
    else{
      this->preco_final=0;
    }
}
float Carrinho::get_preco_final(){
  return this->preco_final;
}

//Verifica se o produto existe, caso exista adiciona ao carrinho
void Carrinho::cadastraProduto(){
  string nome;
  std::cout << "Produto: " << endl;
  cin.ignore();
  getline(cin, nome);
  for(int i=0; i<this->tamanhoCarrinho; i++){
    if(nome==this->lista_de_produtos[i]->get_produto()){
      this->lista_de_produtos[i]->set_quantidade(this->lista_de_produtos[i]->get_quantidade()+1);
      return;
    }
  }
  Produto *meuProduto;
  meuProduto = get_produtoEstoque(nome);
  if(meuProduto==NULL){
    return;
  }
  meuProduto->set_quantidade(1);
  if(meuProduto!=NULL){
    this->lista_de_produtos.push_back(meuProduto);
    this->tamanhoCarrinho++;
    cout << "Produto adicionado ao carrinho" << endl;
  }
}

//Função que remove produto do carrinho
void Carrinho::removeProduto(){
  string nome;
  std::cout << "Produto a ser removido: " << endl;
  cin.ignore();
  getline(cin, nome);
  for(int i=0;i<this->tamanhoCarrinho; i++){
    if(nome==this->lista_de_produtos[i]->get_produto()){
      this->lista_de_produtos.erase(this->lista_de_produtos.begin()+i);
      this->tamanhoCarrinho--;
      std::cout << "Produto removido com sucesso" << endl;
      return;
    }
  }
  std::cout << "OPERAÇÃO INVÁLIDA: Não há produtos no carrinho" << endl;
}

//Função que finaliza a compra
bool Carrinho::efetuaPagamento(){
  int operacao;
  set_preco_final();
  system("clear");
  std::cout << "------ Produtos comprados ------" << endl << endl;
  for(int i=0; i<this->tamanhoCarrinho; i++){
    std::cout << lista_de_produtos[i]->get_produto() << " - R$ " << lista_de_produtos[i]->get_preco() << " - x" << lista_de_produtos[i]->get_quantidade() << endl;
  }
  std::cout << "Valor total da compra: R$ " << get_preco_final() << endl;
  std::cout << "Desconto: R$ " << get_preco_final()*0.15 << endl;
  std::cout << "Valor final da compra: R$ " << get_preco_final()-(get_preco_final()*0.15) << endl;
  std::cout << "---------------------------------------" << endl;
  std::cout << "Digite 1 para finalizar a compra" << endl;
  std::cin >> operacao;
  if(operacao==1){
    Produto* meuProduto;
    for(int i=0; i<this->tamanhoCarrinho; i++){
      meuProduto = get_produtoEstoque(this->lista_de_produtos[i]->get_produto());
      if(meuProduto->get_quantidade()<this->lista_de_produtos[i]->get_quantidade()){
        std::cout << "****** ERROR ******" << endl;
        std::cout << "------ Produto Indisponível ------" << endl;
        cin.ignore();
        getchar();
        return true;
      }
    }
    for(int i=0; i<this->tamanhoCarrinho; i++){
      mudaQuantidade(lista_de_produtos[i]->get_produto(),lista_de_produtos[i]->get_quantidade());
    }
    std::cout << "------ AGRADEÇEMOS A SUA PREFERÊNCIA ------" << endl << endl;
    std::cout << "****** Aperte enter para sair ******" << endl;
    cin.ignore();
    getchar();
    return true;
  }
  else{
    std::cout << "OPERAÇÃO INVÁLIDA: retire o(s) produto(s) do carrinho" << endl;
    return false;
  }
}
